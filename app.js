var express = require("express"),
    app     = express(),
    mongoose = require("mongoose"),
    bodyParser = require("body-parser"),
    passport = require("passport"),
    localStrategy = require("passport-local"),
    expressSession = require("express-session"),
    User = require("./models/user"),
    Comment = require("./models/comment"),
    methodOverride = require('method-override');
    
mongoose.connect("mongodb://localhost/blog_app");


app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static("public"));
app.use(methodOverride('_method'));

// app.use(function(req,res,next){
//     res.locals.currentUser = req.user;
//     return next();
// });

// CONFIGURE PASSPORT
app.use(expressSession({
    secret:"my name is bharat",
    resave:false,
    saveUninitialized:false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new localStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());


var Post = require("./models/post");
var seed = require("./models/seedDB");

// seed();

app.get("/", function(req, res){
    res.redirect("/home");
});

app.get("/home", function(req, res){
    Post.find({},function (err,posts) {
        if (err) {
            console.log(err);
        } else {
            res.render("index",{posts:posts,currentUser:req.user});
        }
    });
    
});

app.get("/home/new",isAdimin, function(req, res){
   res.render("new"); 
});

app.post("/home", function(req, res){
    res.render("/home");
});

app.get("/home/:id",isLoggedIn ,function(req, res){
    Post.findById(req.params.id,function (err,resultPost) {
        if (err) {
            console.log(err);
        } else {
            Comment.find({},function (err,comments) {
                if (err) {
                    console.log(err);
                } else {
                        res.render("show",{resultPost:resultPost,currentUser:req.user,comments:comments});
                   } 
    });
        }
    });
});

app.get("/blogs/:id/edit", function(req, res){
           res.render("edit");
});

app.put("/home/:id", function(req, res){
        //  var showUrl = "/home/" + home._id;
        //  res.redirect(showUrl);
});

app.delete("/blogs/:id", function(req, res){
           res.redirect("/blogs");
});
//Commeent routes

app.get("/home/:id/comment/new",function (req,res) {
    Post.findById(req.params.id,function (err,post) {
        if (err) {
            console.log(err);
        } else {
            res.render("newComment",{post:post,currentUser:req.user});
        }
    });
    
});

app.post("/home/:id/comment",function(req, res) {
   Comment.create(req.body.comment,function (err,comment) {
        if (err) {
            console.log(err);
        } else {
            Post.findById(req.params.id,function(err, post) {
                if (err) {
                    console.log(err);
                } else {
               var author = {id:req.user._id,
                        username:req.user.username
                       };
                        comment.author = author;
                        comment.save();
                        post.push(comment);
                        post.save();
                        res.redirect("/home/"+req.params.id+"/show");     
                }
                        
            });
        }
   }); 
});

//AUTHENTICATION ROUTES

app.get("/register",function(req, res) {
    res.render("register",{currentUser:req.user});
});

app.post("/register",function(req, res) {
    var newUser = new User({username:req.body.username});
    User.register(newUser,req.body.password,function (err,user) {
        if (err) {
            console.log(err);
            return res.render("register");
        } 
            passport.authenticate("local")(req,res,function () {
                res.redirect("/home");
            });
    });
});
// app.get("/login",function(req, res) {
//   res.render("login",{currentUser:req.user}); 
// });


app.post("/login",passport.authenticate("local",{
    successRedirect:"/home",
    failureRedirect:"/"
}) ,function(req, res) {
    
});

app.get("/logout",function(req, res) {
    req.logout();
    res.redirect("/");
});

function isLoggedIn(req,res,next) {
    if(req.isAuthenticated()){
        return next();
    }
    res.send("you must be signed in to do this");
}

function isAdimin(req,res,next) {
    if(req.isAuthenticated()){
        if(req.user.isAdmin){
            return next();
    }}
    res.send("only admin can do this");
}

app.listen(process.env.PORT, process.env.IP,function () {
    console.log("server has started");
});
